const volScalarField rAU("rAU", 1.0/UEqn.A());
volVectorField HbyA("HbyA", U);
HbyA = rAU*UEqn.H();

const surfaceScalarField phiHbyA
(
 "phiHbyA",
 fvc::interpolate(rho) * (fvc::interpolate(HbyA) & mesh.Sf())
);

const volScalarField Dp("Dp", rho*rAU);

fvScalarMatrix pEqn
(
    fvm::ddt(psi, p)
  - fvm::laplacian(Dp, p)
  + fvc::div(phiHbyA)
);

pEqn.solve();
        
phi = phiHbyA + pEqn.flux(); 

U = HbyA - rAU*fvc::grad(p);
U.correctBoundaryConditions();

#include "compressibleContinuityErrs.H"
